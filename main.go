package main

import (
	"fmt"
	"net/http"
)

func murrengan(w http.ResponseWriter, req *http.Request) {
	fmt.Println("murrengan")
	fmt.Fprintf(w, "Привет, муррен!\n")
}

func headers(w http.ResponseWriter, req *http.Request) {
	fmt.Println("headers")
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func main() {
	// пишем путь для вызова функции
	fmt.Println("murr_serve запущен")
	http.HandleFunc("/murrengan/", murrengan)
	http.HandleFunc("/headers/", headers)
	// запускаем сервер на порту
	err := http.ListenAndServe(":1991", nil)
	if err != nil {
		fmt.Println("murr_serve упал:", err)
	}
}
