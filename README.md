<img src="readme/img/thumbnail.png" alt="murr_network_logo" title="Murrengan network"/>

<a href="readme/en">english version</a>


## Как построить процесс доставки приложения в кубернетис используя гитлаб раннер.


Привет!

Меня зовут Егор и мы продолжаем изучать программирование.

Сегодня мы построим процесс доставки приложения в кубернетис используя gitlab ci и runner.

Вроцессе работы мы:
- зарегистрируем гитлаб раннер в кластере кубернетиса через [helm](https://helm.sh/docs/intro/install/)
- создадим веб сервер на голенг
- сбилдим его локальную мультисистемную сборку*
- завернем его в докерфайл
- опишем процесс в .gitlab-ci.yml
- получим доступ к приложению из браузера


Создам проект на https://gitlab.com/, назову его murr_server и возьму токен для гитлаб раннера:

<img src="readme/img/gitlab_ci/gitlab_token_1.png" alt="gitlab_token_1"/>
<img src="readme/img/gitlab_ci/gitlab_token_2.png" alt="gitlab_token_2"/>
<img src="readme/img/gitlab_ci/gitlab_token_3.png" alt="gitlab_token_3"/>

_токен скопировать - вставим его чуть дальше_

Добавляю гитлаб в хелм:
```sh
helm repo add gitlab https://charts.gitlab.io
helm repo update
```

Пропишу ключ раннера в конфиг хелма:
```sh
# выведу содержимое конфига
helm show values gitlab/gitlab-runner > murr-runner.yml
```

Прописываю тег, что бы идентифицировать гитлаб раннер

Проверяю путь к гитлабу

Прописываю токен
```
tags: "murr_runner"
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: "___"
```

Устанавливаю раннер в отдельный неймспейс кубера
```
kubectl create ns gitlab-runner
helm install --namespace gitlab-runner gitlab-runner -f murr-runner.yml gitlab/gitlab-runner
```

Выдаю полные права раннеру*
```
kubectl create clusterrolebinding --clusterrole=cluster-admin -n gitlab-runner --serviceaccount=gitlab-runner:default our-murr-runner
```
Проверю, что раннер запущен:
```
kubectl get po -n gitlab-runner

NAME                                          READY   STATUS    RESTARTS   AGE
gitlab-runner-gitlab-runner-994b96676-bjftj   1/1     Running   0          3m33s
```
Так же мы увидим раннер в настройках гитлаба:

<img src="readme/img/gitlab_ci/gitlab_runner_added.png" alt="gitlab_runner_added"/>

Создам go сервер, который возвращает строку ```Привет, муррен!``` если зайти по url /murrengan/
Проверим локально
```
go run main.go

murr_serve запущен
```
Теперь по адресу http://127.0.0.1:1991/murrengan/ доступно приложение
Откроем в браузере

<img src="readme/img/gitlab_ci/murrengan_hello_message.png" alt="murrengan_hello_message"/>

или проверим в терминале
```
curl http://127.0.0.1:1991/murrengan/
```
<img src="readme/img/gitlab_ci/curl_murrengan.png" alt="curl_murrengan"/>

_Домашнее задание: какие еще пути есть в приложении? Что они делают и как ты думаешь зачем?_

Сделаю исполняемый файл из нашего сервера (пример для windows пользователей)
```
# env GOOS=target-OS GOARCH=target-architecture go build package-import-path

env GOOS=windows GOARCH=amd64 go build -o murr_server.exe
```

Билдить артифакты можно под любые ос:
```
darwin	amd64
linux	arm
windows	amd64
```

Завернем наше приложение в Dockerfile и добавим возможность запуска в контейнере.
Сбилдим имидж:
```
docker build -t murr_server_in_docker:0.3.0 .
```
Проверю готовый имидж
```
docker images
REPOSITORY                  TAG        IMAGE ID       CREATED              SIZE
murr_server_in_docker       0.3.0      18965967f809   About a minute ago   308MB
```

_Домашнее задание: сделать мультистейдж сборку и уменьшить размер контейнера_

Можно запустить и протестировать локально:
```
docker run -it -p 1991:1991 murr_server_in_docker:0.3.0
```

Разместим приложение в кластере кубернетиса. МЕСТО ИНТЕГРАЦИИ С КЛАУД ПРОВАЙДЕРОМ 

Сервис предоставляем мне кубконфиг с доступом к воркер нодам, так же выделяет ip адреса.

Закину config в \.kube\ и проверю активные сервисы в default неймспейсе:
```
kubectl get svc -A          
NAMESPACE     NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                         AGE  
default       kubernetes           ClusterIP   10.96.0.1       <none>        443/TCP                         3h37m
kube-system   kube-dns             ClusterIP   10.96.0.10      <none>        53/UDP,53/TCP                   3h32m
kube-system   kube-state-metrics   NodePort    10.96.27.50     <none>        8080:30080/TCP,8081:30081/TCP   3h32m
kube-system   metrics-server       ClusterIP   10.96.133.247   <none>        443/TCP                         3h32m
```

Опишем инструкцию работы gitlab runner в .gitlab-ci.yml

Когда я начинал программировать, я был против комментариев. Сейчас я понимаю, что комментарии это комментарии - они могут быть нужны.


_Домашнее задание: Добавить стадию тестирования. Прерывать билд, если тест не прошел._

Пушим наши изменения в продакшен ветку - мастер
```
git commit
git push
```
Идем в пайплайны
<img src="readme/img/gitlab_ci/pipeline.png" alt="pipeline"/>

и видим запуск процесса.

СКРИН ЗАПУСКА ПРОЦЕССА (нужен кластер)

Ждем запуск сервиса для доступа к приложению:
```
kubectl get svc -w

NAME                        TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)          AGE
kubernetes                  ClusterIP      10.96.0.1     <none>        443/TCP          3h44m
murr-server-load-balancer   LoadBalancer   10.96.226.1   91.185.84.184 1991:31974/TCP   1h23m
```

Нам надо получить `EXTERNAL-IP` для `murr-server-load-balancer`.
Теперь указав его в браузере, мы получим доступ к нашему приложению из любой точки мира.

СКРИН С ЗАПРОСОМ ИЗ БРАУЗЕРА

Теперь, когда потребуется обновить приложение, мне достаточно сделать новый гит коммит и гит пуш.

Гитлаб сиай подхватит измения, сбилдит новый имидж в раннере у нас в кубере, зальет его в реджестери, обновит и применеи новй деплоймент.

Зайдем на продакшен и увидим новую версию приложения.

В этой статье мы настроили полный процесс развертывания и обновления микросервисного приложения от тачки разработчика, до облака в 2 комманды:

гит коммит

гит пуш
