package main

import (
	"fmt"
	"github.com/go-playground/assert/v2"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMurrengan(t *testing.T) {
	testCase := []struct {
		message string
	}{
		{
			message: "Привет, муррен!",
		},
	}
	handler := http.HandlerFunc(murrengan)

	for _, tc := range testCase {
		t.Run(tc.message, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", fmt.Sprintf("/murrengan/"), nil)
			handler.ServeHTTP(rec, req)
			assert.Equal(t, tc.message, req.Body)
		})
	}
}

func main() {
	//TestMurrengan
}
